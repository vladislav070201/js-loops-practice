// Sum numbers

const calculateSumButtonElement = document.querySelector('#calculator button');

calculateSumButtonElement.addEventListener('click', () => {
  const userNumberInputElement = document.getElementById('user-number');
  const outputResultElement = document.getElementById('calculated-sum');

  const enteredNumber = userNumberInputElement.value;
  let sumUpToNumber = 0;

  for (let i = 0; i <= enteredNumber; i++) {
    sumUpToNumber += i;
  }

  outputResultElement.textContent = String(sumUpToNumber);
  outputResultElement.style.display = 'block';
});

// Highlight links

const highlightLinksButtonElement = document.querySelector('#highlight-links button');

highlightLinksButtonElement.addEventListener('click', () => {
  const anchorElements = document.querySelectorAll('#highlight-links a');

  for (const anchorElement of anchorElements) {
    anchorElement.classList.add('highlight');
  }
});

// Display user data

const dummyUserData = {
  firstName: 'Vlad',
  lastName: 'Yakimovskiy',
  age: 23,
};

const displayUserDataButtonElement = document.querySelector('#user-data button');

displayUserDataButtonElement.addEventListener('click', () => {
  const outputDataElement = document.getElementById('output-user-data');

  outputDataElement.innerHTML = '';

  for (const key in dummyUserData) {
    const newUserDataListElement = document.createElement('li');
    newUserDataListElement.textContent = `${key.toUpperCase()}: ${dummyUserData[key]}`;

    outputDataElement.append(newUserDataListElement);
  }
});

// Statistics / Roll the Dice

const rollDiceButtonElement = document.querySelector('#statistics button');

const rollDice = () => Math.floor(Math.random() * 6) + 1;

rollDiceButtonElement.addEventListener('click', () => {
  const targetNumberInputElement = document.getElementById('user-target-number');
  const diceRollsListElement = document.getElementById('dice-rolls');
  const outputTotalRollsElement = document.getElementById('output-total-rolls');
  const outputTargetNumberElement = document.getElementById('output-target-number');

  const enteredNumber = +targetNumberInputElement.value;
  let hasRolledTargetNumber = false;
  let numberOfRolls = 0;

  diceRollsListElement.innerHTML = '';

  while (!hasRolledTargetNumber) {
    const rolledNumber = rollDice();
    numberOfRolls++;

    const newRollListItemElement = document.createElement('li');
    newRollListItemElement.textContent = `Roll ${numberOfRolls}: ${rolledNumber}`;
    diceRollsListElement.append(newRollListItemElement);

    hasRolledTargetNumber = rolledNumber === enteredNumber;
  }

  outputTargetNumberElement.textContent = String(enteredNumber);
  outputTotalRollsElement.textContent = String(numberOfRolls);
});
